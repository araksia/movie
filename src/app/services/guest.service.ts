import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Jsonp, URLSearchParams } from '@angular/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class GuestService {
  apikey: string;

  constructor(private jsonp: Jsonp) {
    this.apikey = environment.apiKey;
  }
  /**
   * Get guest's session id
   */
  public newGuest() {
    var search = new URLSearchParams();
    search.set('api_key', this.apikey);
    return this.jsonp.get(`${environment.apiEndpoint}authentication/guest_session/new?callback=JSONP_CALLBACK`, { search })
      .pipe(map(res => {
        return res.json();
      }));
  }
}

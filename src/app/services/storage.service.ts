import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  public save(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }
  public exist(key) {
    return localStorage.getItem(key) !== null;
  }
  public read(key) {
    const value = localStorage.getItem(key);
    return JSON.parse(value);
  }

  public remove(key) {
    localStorage.removeItem(key);
  }

  public clear() {
    localStorage.clear();
  }

}
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Jsonp, URLSearchParams, RequestOptions } from '@angular/http';
import { map } from "rxjs/operators";
import { HttpClient, HttpParams } from '@angular/common/http';
import * as Models from '../models';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  apikey: string;
  private headers: any;
  constructor(private jsonp: Jsonp, public http: HttpClient) {
    this.apikey = environment.apiKey;

  }
 /**
  * Get Movie by id
  */
  public getMovie(id: string) {
    var search = new URLSearchParams();
    search.set('api_key', this.apikey);
    return this.jsonp.get(`${environment.apiEndpoint}movie/${id}?callback=JSONP_CALLBACK`, { search })
      .pipe(map(res => {
        return res.json();
      }))
  }
   /**
  * Search movies 
  */
  public searchMovies(searchStr: string, page: string = "1") {
    var search = new URLSearchParams();
    search.set('sort_by', 'popularity.desc');
    search.set('query', searchStr);
    search.set('page', page);
    search.set('api_key', this.apikey);
    return this.jsonp.get(`${environment.apiEndpoint}search/movie?callback=JSONP_CALLBACK`, { search })
      .pipe(map(res => {
        return new Models.Search(res.json());
      }))
  }
 /**
  * Post rate 
  */
  public async rateMovie(movie_id: number, session_id: string, rate: string) {
    this.headers = {
      'Content-Type': 'application/json;charset=utf-8'
    };
    let queryString = `api_key=${this.apikey}&guest_session_id=${session_id}`;

    return new Promise((resolve, reject) => {
      return this.http.post(`${environment.apiEndpoint}movie/${movie_id}/rating?${queryString}`, { value: rate }, { headers: this.headers })
        .subscribe(response => {
          resolve(response);
        }, err => {
          reject(err);
        });
    });
  }
}

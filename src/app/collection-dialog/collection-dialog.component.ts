import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StorageService } from '../services/storage.service';
import * as Models from '../models';

@Component({
  selector: 'app-collection-dialog',
  templateUrl: './collection-dialog.component.html',
  styleUrls: ['./collection-dialog.component.scss']
})
export class CollectionDialogComponent implements OnInit {
  public model: Array<Models.Collection> = new Array<Models.Collection>();
  public title: string = '';
  public description: string = '';
  public message: string;
  constructor(public dialogRef: MatDialogRef<CollectionDialogComponent>, private storageService: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.model = this.storageService.read("Collection");

  }
  public closeDialog() {
    this.dialogRef.close();
  }

  //Save Collection at localstorage
  public saveCollection() {
    let newData = [];
    const previusData = this.storageService.read('Collection');
    if (!previusData) {
      newData.push({ 'title': this.title, 'description': this.description, 'movies': this.data.movie });
      this.storageService.save('Collection', newData);
    } else {
      previusData.push({ 'title': this.title, 'description': this.description, 'movies': this.data.movie });
      this.storageService.remove('Collection');
      this.storageService.save('Collection', previusData);
    }

    this.message = "Your collection was saved!";
    this.dialogRef.close();
  }

  //add movies to collection
  public addMovies(title: string) {
    let newMovies = [];

    const previusData = this.storageService.read('Collection');
    let willChange = previusData.find(x => x.title === title);
    const notChange = previusData.filter(function (obj) {
      return obj.title !== title;
    });
    const movies = willChange.movies;
    newMovies.push(movies);
    newMovies.push(this.data);
    notChange.push({ 'title': willChange.title, 'description': willChange.description, 'movies': newMovies });
    this.storageService.remove('Collection');
    this.storageService.save('Collection', notChange);
    this.dialogRef.close();
  }
}

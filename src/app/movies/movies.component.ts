import { Component, OnInit } from '@angular/core';
import * as Models from '../models';
import { GuestService } from '../services/guest.service';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  guest: Models.Guest;
  guestExist: boolean = false;
  constructor(private guestService: GuestService, private storageService: StorageService) {
  }

  ngOnInit() {
    this.guestExist = this.storageService.exist('guest');
    if (!this.guestExist) {
      this.guestService.newGuest().subscribe(res => {
        this.guest = new Models.Guest(res);
        if (this.guest.success) {
          this.storageService.save('guest', this.guest.guest_session_id);
        }
      });
    }
  }

}
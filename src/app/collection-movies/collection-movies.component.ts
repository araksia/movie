import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../services/storage.service';
import * as Models from '../models';

@Component({
  selector: 'app-collection-movies',
  templateUrl: './collection-movies.component.html',
  styleUrls: ['./collection-movies.component.scss']
})
export class CollectionMoviesComponent implements OnInit {
  public model: Models.Collection;
  constructor(private router: ActivatedRoute, private storageService: StorageService) { }
  public title:string;

  ngOnInit() {
    this.router.params.subscribe((params) => {
      this.title = params['title'];
      const allData = this.storageService.read('Collection');
      this.model = new Models.Collection(allData.find(x => x.title === this.title));
      });
  }
  public deleteMovie(id: number, title:string){

    const previusData = this.storageService.read('Collection');
    const willChange = previusData.find(x => x.title === title);
    const notChange = previusData.filter(function (obj) {
      return obj.title !== title;
    });
    const movies = willChange.movies;
    const removeMovie = movies.filter(function (obj) {
      return obj.id !== id;
    });

    notChange.push({ 'title': willChange.title, 'description': willChange.description, 'movies': removeMovie });
    this.storageService.remove('Collection');
    //update storage data
    this.storageService.save('Collection', notChange);
    const allData = this.storageService.read('Collection');
    this.model = new Models.Collection(allData.find(x => x.title === title));
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../services/movies.service';
import * as Models from '../models';
import { StarRatingComponent } from 'ng-starrating';
import { StorageService } from '../services/storage.service';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']

})
export class MovieComponent implements OnInit {
  movie: Models.Movie = new Models.Movie();
  guest: string;
  session_id: string;
  newValue: any;
  public message: string = "";
  constructor(
    private moviesServices: MoviesService,
    private router: ActivatedRoute, private storageService: StorageService
  ) {
  }

  ngOnInit() {
    //get if from router
    this.router.params.subscribe((params) => {
      const id = params['id'];
      this.moviesServices.getMovie(id).subscribe(res => {
        this.movie = new Models.Movie(res);
      });
    })
  }

  public async onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    this.guest = this.storageService.read('guest');
    if (this.guest) {
      this.newValue = await this.moviesServices.rateMovie(this.movie.id, this.guest, ($event.newValue * 2).toString());
      if (this.newValue && this.newValue.status_code === 12) {
        this.message = "Your rate was updated successfully."
      } else if (this.newValue && this.newValue.status_code === 1) {
        this.message = "Your rate was saved successfully."
      } else {
        this.message = "Your rate wasn't save."
      }
    }
  }
}

import * as Models from '../models';

export class Search {

    page:number;
    results: Models.Movie;
    total_pages:number;
    total_results:number;
    
    constructor();
    constructor(obj: any);
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.page = obj.page;
        this.results = obj.results;
        this.total_pages = obj.total_pages;
        this.total_results = obj.total_results;

    }
}
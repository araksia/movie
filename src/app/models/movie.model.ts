export class Movie {

    budget: number;
    id: number;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: string;
    revenue: number;
    spoken_languages: any=[];
    title: string;
    vote_average: number;
    vote_count: number;

    constructor();
    constructor(obj: any);
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.budget = obj.budget;
        this.id = obj.id;
        this.overview = obj.overview;
        this.popularity = obj.popularity;
        this.poster_path = obj.poster_path;
        this.release_date = obj.release_date;
        this.revenue = obj.revenue;
        this.spoken_languages = obj.spoken_languages;
        this.title = obj.title;
        this.vote_average = obj.vote_average;
        this.vote_count = obj.vote_count;
    }
}
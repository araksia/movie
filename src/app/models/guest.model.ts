export class Guest {

    success:boolean;
    guest_session_id: string;
    expires_at:string;


    constructor();
    constructor(obj: any);
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.success = obj.success;
        this.guest_session_id = obj.guest_session_id;
        this.expires_at = obj.expires_at;


    }
}
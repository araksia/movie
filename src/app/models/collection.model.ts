import * as Models from '.';
export class Collection {

    title: string;
    description: string;
    movies: Models.Movie;
    constructor();
    constructor(obj: any);
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.title = obj.title;
        this.description = obj.description;
        this.movies = obj.movies;

    }
}
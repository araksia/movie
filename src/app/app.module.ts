import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchComponent } from './search/search.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';
import { MovieInnerComponent } from './movie-inner/movie-inner.component';
import { MatIconModule, MatPaginatorModule } from '@angular/material';
import { StorageService } from './services/storage.service';
import { RatingModule } from 'ng-starrating';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CollectionDialogComponent } from './collection-dialog/collection-dialog.component';
import { CollectionComponent } from './collection/collection.component';
import { CollectionMoviesComponent } from './collection-movies/collection-movies.component';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MoviesComponent,
    MovieComponent,
    MovieInnerComponent,
    CollectionDialogComponent,
    CollectionComponent,
    CollectionMoviesComponent,
  ],
  imports: [
    BrowserModule,
    RatingModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    JsonpModule,
    FormsModule,
    MatIconModule,
    MatPaginatorModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [StorageService,  NgbModal, MatDialog],
  bootstrap: [AppComponent],
  entryComponents: [CollectionDialogComponent],
})
export class AppModule { }

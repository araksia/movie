import { Component, Input } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { CollectionDialogComponent } from '../collection-dialog/collection-dialog.component';
import * as Models from '../models';

@Component({
  selector: 'app-movie-inner',
  templateUrl: './movie-inner.component.html',
  styleUrls: ['./movie-inner.component.scss']
})
export class MovieInnerComponent {

  @Input()
  movie: Models.Movie = new Models.Movie();
  constructor(public dialog: MatDialog) {}  
  

  public addToCollection(): void {
    //open dialog
    const dialogRef = this.dialog.open(CollectionDialogComponent, {
      width: '250px',
      data :{'movie':this.movie}
    });

    dialogRef.afterClosed().subscribe(result => {
    
    });
  }
}
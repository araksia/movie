import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieInnerComponent } from './movie-inner.component';

describe('MovieInnerComponent', () => {
  let component: MovieInnerComponent;
  let fixture: ComponentFixture<MovieInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

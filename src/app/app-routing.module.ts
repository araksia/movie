import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';
import { CollectionComponent } from './collection/collection.component';
import { CollectionMoviesComponent } from './collection-movies/collection-movies.component';


const routes: Routes = [
  { path: '', redirectTo: '/movies', pathMatch: 'full' },
  { path: 'movies', component: MoviesComponent },
  { path: 'collection', component: CollectionComponent },
  { path: 'movie/:id', component: MovieComponent },
  { path: 'collection-movies/:title', component: CollectionMoviesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

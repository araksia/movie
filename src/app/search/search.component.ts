import { Component, OnInit, ViewChild, Directive} from '@angular/core';
import { MoviesService } from '../services/movies.service';
import {  MatPaginator } from '@angular/material';  
import * as Models from '../models';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
@Directive({
  selector: '[appSearch]',
  exportAs: 'appSearch'
})
export class SearchComponent implements OnInit {

  searchRes: Models.Movie;
  searchStr: string;
  currentPage: number;
  totalResults: number;
  totalPages: number;
  public model: Models.Search = new Models.Search();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private moviesService: MoviesService) {
  }
  ngOnInit() {

  }

  searchMovies() {

    this.moviesService.searchMovies(this.searchStr).subscribe(res => {
      this.model = new Models.Search(res);
    })
  }
  //get next set of movies for pagination
  getNext(event){
    let nextPage = event.pageIndex; 
    if(nextPage <1 ){
      return;
    }
    if(nextPage <= this.totalPages){
    this.moviesService.searchMovies(this.searchStr, nextPage).subscribe(res => {
      this.model = new Models.Search(res)
    })
  }
  }

  //validation for 3 charachters
  keyUp(event) {
    if (event.keyCode != 13) {
      return;
    }
    if (this.searchStr && this.searchStr.length > 2 && this.isAlphaNumeric(this.searchStr)) {
      this.searchMovies();
    }
  }
  
  //alphanumeric
  isAlphaNumeric = function (value: string) {
    var regExp = /^[A-Za-z0-9]+$/;
    return regExp.test(value);
  };



}
import { Component, OnInit } from '@angular/core';
import * as Models from '../models';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {
  public model: Array<Models.Collection> = new Array<Models.Collection>();
  public title: string = '';
  public description: string = '';
  public message: string = '';
  constructor(private storageService: StorageService) { }

  ngOnInit() {

    this.model = this.storageService.read("Collection");
  }
  //Save Collection and update modal
  public saveCollection() {
    let newData = [];
    const previusData = this.storageService.read('Collection');
    if(!previusData){
      newData.push({ 'title': this.title, 'description': this.description, 'movies': {} });
      this.storageService.save('Collection', newData);
    } else{
      previusData.push({ 'title': this.title, 'description': this.description, 'movies': {} });
      this.storageService.remove('Collection');
      this.storageService.save('Collection', previusData);
    }
 
    this.message = "Your collection was saved!";
    this.model = this.storageService.read("Collection");
  }

}
